using System;
using System.ComponentModel.DataAnnotations;

namespace taller_mvc.Models
{
    public class FormContactModel
    {
        [Required]
        public string Name { get; set; }
        
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        
        [Required]
        public string Subject { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 10, ErrorMessage = "La longitud del mensaje debe ser mayor a 10 y menor a 100 caracteres.")]
        [DataType(DataType.MultilineText)]
        public string Message { get; set; }
    }
}
