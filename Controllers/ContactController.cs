using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using taller_mvc.Models;

namespace taller_mvc.Controllers
{
    public class ContactController: Controller
    {
        [HttpPost]
        public IActionResult Index([FromBody] FormContactModel model) {
            var response = new ContactResponse();
            if(model == null)
            {
                response.Success = false;
            }
            else 
            {
                response.Success = true;
            }
            return Ok(response);
        }
    }
}